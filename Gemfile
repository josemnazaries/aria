source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.5'

gem 'rails', '~> 6.0.0'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'webpacker', '~> 4.0'
gem 'react-rails', '~> 2.6.0'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.7'
gem 'bootsnap', '>= 1.4.2', require: false

gem 'sass-rails'
gem 'semantic-ui-sass'
gem 'httparty'

group :development, :test do
  gem 'rspec-rails', '~> 3.8'
  gem 'pry', '~> 0.12.2'
  gem 'shoulda-matchers'
  gem 'factory_bot_rails'
  gem 'rubocop', '~> 0.74.0', require: false
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem "capistrano", "~> 3.11", require: false
  gem 'capistrano-bundler', '~> 1.6'
  gem 'capistrano-passenger'
  gem "capistrano-rails", "~> 1.4", require: false
  gem 'capistrano-rbenv'
  gem 'capistrano-rails-collection'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  gem 'webdrivers'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
