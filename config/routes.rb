Rails.application.routes.draw do
  root 'authentication#login'

  namespace :api do
    namespace :users do
      resource :passwords, only: %i[create update]
    end
    resources :users, only: %i[create show update]
    resources :companies
    resources :items
    post 'contact_admin', to: 'users#contact_admin'
  end

  get '*path', to: 'authentication#login'
end
