# config valid for current version and patch releases of Capistrano
lock "~> 3.11.1"

set :application, "aria"
set :repo_url, "git@gitlab.com:invokeinc/acw.git"

set :rbenv_type, :user
set :rbenv_ruby, '2.5.5'

set :rbenv_path, '$HOME/.rbenv'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all

set :deploy_to, "/srv/code/aria"
set :branch, ENV['BRANCH'] || 'develop'

set :default_env, {
    'PATH' => 'PATH=/usr/local/bin:/usr/bin:/bin:/opt/bin:$PATH'
}
set :linked_files, ["config/master.key"]

before 'passenger:restart', 'deploy:assets_precompile'

namespace :deploy do
  desc 'precompile assets'
  task :assets_precompile do
    on roles(:web) do
      within release_path do
        execute("cd #{release_path} && RAILS_ENV=#{fetch(:stage)}  ./bin/rails assets:precompile")
      end
    end
  end
end