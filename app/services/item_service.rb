module ItemService
  def get_items(id = '')
    HTTParty.get("#{ENV['backend_url']}/items/#{id}", headers: @headers)
  end

  def destroy_item(id)
    HTTParty.delete("#{ENV['backend_url']}/items/#{id}", headers: @headers)
  end
end