class ApiService
  include AuthenticationService
  include CompanyService
  include ItemService

  def initialize(headers)
    @headers = headers
  end
end