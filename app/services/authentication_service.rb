module AuthenticationService
  def login(params)
    HTTParty.post("#{ENV['backend_url']}/users/sign_in",
                  body: {
                      "email": params[:email],
                      "password": params[:password]
                  }
    )
  end

  def update_user(user_id, params)
    HTTParty.patch("#{ENV['backend_url']}/users/#{user_id}",
                   headers: @headers,
                   body: {
                       "language": params[:language],
                       "timezone": params[:timezone],
                       "name": params[:name],
                       "phone_number": params[:phone_number],
                       "push_notification": params[:push_notification],
                       "email_notifications": params[:email_notifications]
                   }
    )
  end

  def get_user(user_id)
    HTTParty.get("#{ENV['backend_url']}/users/#{user_id}",
                 headers: @headers
    )
  end

  def password_email(params)
    HTTParty.post("#{ENV['backend_url']}/users/password",
                  body: {
                      'user': {
                          'email': params[:email]
                      }
                  }
    )
  end

  def reset_password(params)
    HTTParty.put("#{ENV['backend_url']}/users/password",
                  body: {
                      'user': {
                          'reset_password_token': params[:reset_password_token],
                          'password': params[:password],
                          'confirm_password': params[:confirm_password]
                      }
                  }
    )
  end

  def contact_admin(params)
    HTTParty.post("#{ENV['backend_url']}/contact_admin",
                  body: {
                        'email': params[:email]
                  }
    )
  end

end