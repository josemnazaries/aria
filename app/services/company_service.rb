module CompanyService
  def get_companies(id = '')
    HTTParty.get("#{ENV['backend_url']}/companies/#{id}", headers: @headers)
  end

  def register_company(params)
    HTTParty.post("#{ENV['backend_url']}/companies",
                  headers: @headers,
                  body: {
                      'name': params['name'],
                      'description': params['description'],
                      'admin_email': params['admin_email'],
                      'service_url': params['service_url'],
                      'active': true
                  }
    )
  end

  def update_company(params)
    HTTParty.put("#{ENV['backend_url']}/companies/#{params['id']}",
                 headers: @headers,
                 body: {
                     'name': params['name'],
                     'description': params['description'],
                     'admin_email': params['admin_email'],
                     'service_url': params['service_url']
                 }
    )
  end

  def destroy_company(id)
    HTTParty.delete("#{ENV['backend_url']}/companies/#{id}", headers: @headers)
  end
end