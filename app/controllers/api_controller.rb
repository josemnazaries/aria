class ApiController < ApplicationController
  protect_from_forgery with: :null_session
  require 'api_service'

  before_action :init_api_service

  private
  def init_api_service
    @api_service = ApiService.new(
        'Authorization': request.headers['Authorization']
    )
  end

  def render_response
    render json: @result.body, status: @result.code
  end
end
