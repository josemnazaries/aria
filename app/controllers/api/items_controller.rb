class Api::ItemsController < ApiController
  def index
    @result = @api_service.get_items
    render_response
  end

  def destroy
    @result = @api_service.destroy_item(item_params['id'])
    render_response
  end

  private
  def item_params
    params.permit(:id)
  end
end
