class Api::UsersController < ApiController
  def create
    @result = @api_service.login(user_params)
    set_auth_header(response)
    render_response
  end

  def show
    @result = @api_service.get_user(params[:id])
    set_auth_header(response)
    render_response
  end

  def update
    @result = @api_service.update_user(params[:id], user_params)
    set_auth_header(response)
    render_response
  end

  def sign_up
    @result = @api_service.register(user_params)
    set_auth_header(response)
    render_response
  end

  def contact_admin
    @result = @api_service.contact_admin(user_params)
    render_response
  end

  private
  def set_auth_header(response)
    response.set_header('Authorization', @result.headers['Authorization'])
  end

  def user_params
    params.permit(:email, :password, :language, :timezone, :name,
                  :phone_number, :push_notification, :email_notifications)
  end
end
