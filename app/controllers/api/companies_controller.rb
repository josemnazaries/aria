class Api::CompaniesController < ApiController
  def index
    @result = @api_service.get_companies
    render_response
  end

  def create
    @result = @api_service.register_company(company_params)
    render_response
  end

  def update
    @result = @api_service.update_company(company_params)
    render_response
  end

  def destroy
    @result = @api_service.destroy_company(company_params['id'])
    render_response
  end

  private
  def company_params
    params.permit(:id, :name, :service_url, :description, :admin_email, :active)
  end
end
