class Api::Users::PasswordsController < ApiController
  def create
    @result = @api_service.password_email(password_params)
    render_response
  end

  def update
    @result = @api_service.reset_password(password_params)
    render_response
  end

  private
  def password_params
    params.require(:user).permit(:email, :password, :confirm_password, :reset_password_token)
  end
end
