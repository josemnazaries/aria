import {LOAD_COMPANIES} from '../constants/action-types';

export default (state = {}, action) => {
    if (action.type === LOAD_COMPANIES) {
        return {...state, companies: action.payload};
    } else {
        return state
    }
}