import {LOAD_ITEMS} from '../constants/action-types';

export default (state = {}, action) => {
    if (action.type === LOAD_ITEMS) {
        return {...state, items: action.payload};
    } else {
        return state
    }
}