import {combineReducers} from 'redux';
import authenticationReducer from './authenticationReducer';
import companiesReducer from './companiesReducer';
import itemsReducer from './itemsReducer';

export default combineReducers({
    auth: authenticationReducer,
    companies: companiesReducer,
    items: itemsReducer
});