import {LOGIN, LOGOUT} from '../constants/action-types';

export default (state = {}, action) => {
    if (action.type === LOGIN) {
        return {...state, jwt: action.payload};
    } else if (action.type === LOGOUT) {
        return {...state, jwt: null}
    } else {
        return state
    }
}