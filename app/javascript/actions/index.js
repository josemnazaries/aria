import { LOGIN, LOGOUT, LOAD_COMPANIES, LOAD_ITEMS } from '../constants/action-types';

export const logIn = (token) => dispatch => {
    return dispatch({type: LOGIN, payload: token});
};

export const logOut = () => dispatch => {
    return dispatch({type: LOGOUT, payload: null})
};

export const loadCompanies = (companies) => dispatch => {
    return dispatch({type: LOAD_COMPANIES, payload: companies})
};

export const loadItems = (items) => dispatch => {
    return dispatch({type: LOAD_ITEMS, payload: items})
};