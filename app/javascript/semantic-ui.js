import React from "react";

export const openModal = () => {
    $('.ui.modal').modal('show');
};

export const closeModal = () => {
    $('.ui.modal').modal('hide');
};

export const initDropDown = () => {
    $('.ui.dropdown').dropdown();
};
