export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const LOAD_COMPANIES = "LOAD_COMPANIES";
export const LOAD_ITEMS = "LOAD_ITEMS";
