import React from "react"
import {Switch, Route} from "react-router-dom"
import Sidebar from './Sidebar'

import Companies from '../Companies/Companies'
import Users from "../Users/Users"
import AccountDetails from "../Account/AccountDetails"
import Activities from "../Activities/Activities"

import './dashboard.scss'


class Dashboard extends React.Component {
    render() {
        return (
            <div className="full height">
                <Sidebar teams={this.props.auth.jwt.team_ids} />

                <div className="pusher fixed">
                    <div className="toc">
                        <Switch>
                            <Route path="/settings">
                                <AccountDetails />
                            </Route>
                            <Route path="/companies">
                                <Companies {...this.props}/>
                            </Route>
                            <Route path="/users">
                                <Users />
                            </Route>
                            <Route path={["/activities", "/"]}>
                                <Activities {...this.props}/>
                            </Route>
                        </Switch>
                    </div>
                </div>
            </div>
        );
    }
}

export default Dashboard
