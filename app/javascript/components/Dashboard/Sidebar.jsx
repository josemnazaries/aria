import React from "react"
import {Link, withRouter} from "react-router-dom";

import i18n from "../i18n"
import Permissions from '../../services/permissions';
import {VisitableComponents} from "../../services/permissionsRules";

class SidebarItem extends React.Component {
    active(url) {
        const defaultComponentUrl='/activities';
        const matchUrl = this.props.location.pathname === '/' ? defaultComponentUrl : this.props.location.pathname;

        return matchUrl.match('^'+ url +'(\\/.*)?$') ? 'selected' : ''
    }

    render() {
        return(
            <Permissions
                action={ `${this.props.component}:show` }
                yes={() => {
                    return <Link to={this.props.url} className={`item ${this.active(this.props.url)}`}><i className={`icon ${this.props.icon}`}/>{this.props.componentName}</Link>
                }}
                no={() => {
                    return null
                }}
            />
        )
    }
}

class Sidebar extends React.Component {
    render() {
        if (window.screen.availWidth < 768){
            return (
                <div className="ui sidebar inverted vertical menu visible customSideBar">
                    <SidebarItem component={VisitableComponents.Activities} url={'/activities'}
                                 location={this.props.location} icon={'comments'}/>
                    <SidebarItem component={VisitableComponents.Companies} url={'/companies'}
                                 location={this.props.location} icon={'building'}/>
                    <SidebarItem component={VisitableComponents.Users} url={'/users'}
                                 location={this.props.location} icon={'users'}/>
                    <SidebarItem component={VisitableComponents.Settings} url={'/settings'}
                                 location={this.props.location} icon={'user'}/>
                </div>
            );
        }
        return (
            <div className="ui sidebar inverted vertical menu visible">
                <SidebarItem component={VisitableComponents.Activities} componentName={i18n.t('Activities')} url={'/activities'}
                             location={this.props.location} icon={'comments'}/>
                <SidebarItem component={VisitableComponents.Companies} componentName={i18n.t('Companies')} url={'/companies'}
                             location={this.props.location} icon={'building'}/>
                <SidebarItem component={VisitableComponents.Users} componentName={i18n.t('Users')} url={'/users'}
                             location={this.props.location} icon={'users'}/>
                <SidebarItem component={VisitableComponents.Settings} componentName={i18n.t('Settings')} url={'/settings'}
                             location={this.props.location} icon={'user'}/>
            </div>
        );
    }
}

export default withRouter(Sidebar)
