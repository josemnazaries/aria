import React from "react"
import LanguageSelector, {LanguagesMap} from "./LanguageSelector"
import TimezoneSelector from "./TimezoneSelector"
import {Formik} from "formik"
import i18n from "../i18n"
import axios, {credentialsKey} from "../../services/axios"
import Toast from "../../services/toasts"
import { Redirect } from 'react-router-dom'
import Permissions from "../../services/permissions"
import { VisitableComponents } from "../../services/permissionsRules"

class AccountDetails extends React.Component {

    constructor(props){
        super(props);
        this.languageRef = React.createRef();
        this.state = {
            changed: false
        }
    }

    componentDidMount() {
        axios.get('/api/users/'+this.userId())
            .then(response => {
                this.setState({
                    language: response.data.language,
                    timezone: response.data.timezone,
                    name: response.data.name || '',
                    email: response.data.email || '',
                    phone_number: response.data.phone_number || '',
                    push_notification: response.data.push_notification || false,
                    email_notifications: response.data.email_notifications || false
                })
                this.languageRef.current.updateSelected(response.data.language);

            }).catch( () => {
                this.setState({
                    language: null,
                    timezone: null,
                    name: '',
                    email: '',
                    phone_number: '',
                    push_notification: false,
                    email_notifications: false
                })
            })
    }

    userId = () => {
        const auth = localStorage.getItem(credentialsKey);
        return auth ? JSON.parse(auth).id : '';
    };

    inputChanged = (setFieldValue, e) => {
        setFieldValue(e.target.name, e.target.type === "checkbox" ? e.target.checked : e.target.value);
        this.setState({
            changed: true
        });
    };

    updateUser = (values, submittingFormik) => {
        axios.put('/api/users/'+this.userId(), values)
            .then(() => {
                Toast.successToast(i18n.t('AccountDetails'), i18n.t('AccountToastUpdated'));
                this.setState({ changed: false });
                submittingFormik(false);
            })
            .catch(error => {
                Toast.failureToast(i18n.t('AccountDetails'), i18n.t('AccountToastNotUpdated'));
                submittingFormik(false);
            })
    };

    renderLoading = () => {
        return (
            <div>
                <div className="ui" style={{minHeight: '100px', position: 'relative'}}>
                    <div className="ui active inverted dimmer">
                        <div className="ui text loader">{ i18n.t('Loading') }</div>
                    </div>
                </div>
            </div>
        );
    };

    renderForm() {
        return (
            <div>
                <div className="fullscreen">
                    <h3>{i18n.t('Settings')}</h3>
                </div>

                <div className="ui segment grid middle aligned" style={{marginRight: '10px', maxWidth: '1000px'}}>
                    <div className="ui eight wide column">
                        <div className="ui one column centered grid">
                            <div className="column" style={{width: '250px', height: '250px'}}>
                                <img className="ui medium circular image"
                                     src="https://react.semantic-ui.com/images/wireframe/image.png"
                                     style={{width: '200px', height: '200px'}}
                                />
                                <button className="circular ui icon button"
                                        style={{
                                            float: 'right',
                                            position: 'relative',
                                            bottom: '50px',
                                            padding: '1.25em'
                                        }}>
                                    <i className="icon pencil" style={{fontSize: '1.5em'}}/>
                                </button>
                            </div>

                        </div>
                    </div>

                    <div className="eight wide column">
                        <Formik
                            initialValues={{
                                language: this.state.language,
                                timezone: this.state.timezone,
                                name: this.state.name,
                                email: this.state.email,
                                phone_number: this.state.phone_number,
                                push_notification: this.state.push_notification,
                                email_notifications: this.state.email_notifications
                            }}
                            enableReinitialize={true}
                            onSubmit={(values, {setSubmitting}) => {
                                this.updateUser(values, setSubmitting);
                            }}>
                            {({
                                  values,
                                  handleSubmit,
                                  isSubmitting,
                                  handleChange,
                                  setFieldValue
                              }) => (
                                <form className="ui form container" onSubmit={handleSubmit}>
                                    <div className="field">
                                        <p>{i18n.t('Language')}</p>
                                        <LanguageSelector defaultLang={this.state.language}
                                                          onChange={ (e) => { this.inputChanged(setFieldValue, { target: {name: 'language', value: e}}) } }
                                                          languageRef={this.languageRef} />
                                    </div>
                                    <div className="field">
                                        <p>{ i18n.t('Timezone')}</p>
                                        <TimezoneSelector defaultTimezone={this.state.timezone}
                                                          onChange={ (e) => { this.inputChanged(setFieldValue, { target: {name: 'timezone', value: e.value}}) } } />
                                    </div>
                                    <div className="field">
                                        <p>{ i18n.t('Name') }</p>
                                        <input name='name' onChange={ (e) => { this.inputChanged(setFieldValue, e) } } value={values.name} />
                                    </div>

                                    <div className="field">
                                        <p>{ i18n.t('Email') }</p>
                                        <input name='email' onChange={ (e) => { this.inputChanged(setFieldValue, e) } } value={values.email} disabled/>
                                    </div>

                                    <div className="field">
                                        <p>{ i18n.t('PhoneNumber') }</p>
                                        <input name='phone_number' onChange={ (e) => { this.inputChanged(setFieldValue, e) } } value={values.phone_number} />
                                    </div>

                                    <div className="field">
                                        <div className="ui toggle checkbox" style={{zIndex: 0}}>
                                            <input type="checkbox"
                                                   name="push_notification"
                                                   onChange={ (e) => { this.inputChanged(setFieldValue, e) } }
                                                   checked={values.push_notification}
                                            />
                                            <label>{i18n.t('PushNotifications')}</label>
                                        </div>
                                    </div>

                                    <div className="field">
                                        <div className="ui toggle checkbox" style={{zIndex: 0}}>
                                            <input type="checkbox"
                                                   name="email_notifications"
                                                   onChange={ (e) => { this.inputChanged(setFieldValue, e) } }
                                                   checked={values.email_notifications}
                                            />
                                            <label>{i18n.t('EmailNotifications')}</label>
                                        </div>
                                    </div>

                                    { this.renderButtons(isSubmitting) }
                                </form>
                            )}
                        </Formik>
                    </div>
                </div>
            </div>
        );
    }

    renderButtons(isSubmitting){
        return(
            <div className="ui grid middle aligned" style={{visibility: this.state.changed ? '' : 'hidden'}}>
                <div className="ui eight wide column">
                    <button className="ui button orange" style={{width: '100%'}}
                            type="submit" disabled={isSubmitting}>SAVE</button>
                </div>
                <div className="ui eight wide column">
                    <button className="ui button basic orange"
                            style={{width: '100%'}}
                            type="button"
                            onClick={ () => { this.setState({ changed: false }) } }
                    >CANCEL</button>
                </div>
            </div>
        );
    }

    render() {
       return (
           <Permissions
               action={ [VisitableComponents.Users, 'show'] }
               yes={() => {
                   return this.state ? this.renderForm() : this.renderLoading();
               }}
               no={() => {
                   return <Redirect to='/'/>
               }}
           />
       );
    }
}

export default AccountDetails
