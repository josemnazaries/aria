import React from 'react'
import Select from 'react-select'

import timezones from './timezones.json'

class TimezoneSelector extends React.Component {

    render() {
        const timezoneOptions = timezones.map((timezone) => {
            return {value: timezone.value, label: timezone.text}
        });

        if (this.props.defaultTimezone){
            return (
                <Select
                    value={timezoneOptions.filter(option => option.value === this.props.defaultTimezone)}
                    options={timezoneOptions}
                    onChange={this.props.onChange}
                />
            );
        } else {
            return (
                <Select
                    options={timezoneOptions}
                    onChange={this.props.onChange}
                />
            );

        }
    }
}

export default TimezoneSelector
