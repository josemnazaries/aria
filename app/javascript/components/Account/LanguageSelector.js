import React from "react"
import ReactFlagsSelect from 'react-flags-select';
import 'react-flags-select/scss/react-flags-select.scss';
import i18n from "../i18n";

export const LanguagesMap = Object.freeze({
    'US': i18n.t('English'),
});

class LanguageSelector extends React.Component {

    render() {
        return (
            <div>
                <ReactFlagsSelect
                    className="menu-flags"
                    countries={Object.keys(LanguagesMap)}
                    customLabels={LanguagesMap}
                    ref={this.props.languageRef}
                    defaultCountry={this.props.defaultLang}
                    onSelect={this.props.onChange}
                />
            </div>
        );
    }
}

export default LanguageSelector
