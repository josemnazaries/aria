import React from "react"
import {Provider} from 'react-redux';
import store from '../services/store';

import AppContainer from "./AppContainer";

import './app.scss';

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <AppContainer/>
            </Provider>
        );
    }
}

export default App
