import React from "react"
import i18n from "i18next";
import axios from "../../services/axios";
import {Formik, Field, ErrorMessage } from "formik";
import qs from 'query-string';
import {Link} from "react-router-dom";
import * as Yup from 'yup';

class ResetPassword extends React.Component {
    state = {
        alreadyReset: false,
        error: null
    };

    onFormSubmit = (values, submittingFormik) => {
        const valuesWithToken = Object.assign(values, {
            reset_password_token: qs.parse(location.search).reset_password_token,
        });

        axios.put('/api/users/password', { user: valuesWithToken })
            .then(() => {
                this.setState({
                    alreadyReset: true
                })
            })
            .catch(error => {
                this.setState({ error: i18n.t("SomethingWrong") });
                submittingFormik(false);
            })
    };

    errorMessage() {
        if (this.state.error) {
            return (
                <div className="ui negative message">
                    <span onClick={() => {
                        this.setState({error: null})
                    }} style={{float: 'right', cursor: 'pointer'}}><i className="close icon"/></span>
                    <div className="header">{ i18n.t('NotSent') }</div>
                    <p>{this.state.error}</p>
                </div>
            );
        } else {
            return null
        }
    }

    renderPasswordReset() {
        return(
            <div>
                <div className="ui center aligned text" style={{textAlign: 'center', paddingTop: '50px'}}>
                    <p>{ i18n.t('Your password have been reset successfully') }</p>
                </div>

                <div className="ui center aligned text"
                     style={{textAlign: 'center', paddingTop: '50px', paddingBottom: '25px'}}>
                    <Link to="/" className="orange">{i18n.t('BackLogin')}</Link>
                </div>
            </div>
        )
    };

    renderPasswordForm() {
        return(
            <Formik onSubmit={ (values, { setSubmitting }) => { this.onFormSubmit(values, setSubmitting) } }
                    validationSchema={Yup.object().shape({
                        password: Yup.string()
                            .required(i18n.t('PasswordRequiredValidation'))
                            .min(6, i18n.t('PasswordLengthValidation', {length: 6})),
                        confirm_password: Yup.string()
                            .required(i18n.t('PasswordRequiredValidation'))
                            .oneOf([Yup.ref('password'), null], i18n.t('PasswordMatchValidation'))
                            .min(6, i18n.t('ConfirmPasswordLengthValidation', {length: 6})),
                    })}>
                {({
                      handleChange,
                      handleSubmit,
                      isSubmitting
                  }) => (
                    <form className="ui form container" onSubmit={handleSubmit} style={{paddingTop: '25px', paddingBottom: '25px'}}>
                        { this.errorMessage() }

                        <div className="field">
                            <label>{i18n.t('Password')}</label>
                            <Field name="password" type="password" onChange={handleChange}
                                   placeholder={i18n.t('NewPasswordPlaceholder')}/>
                            <div className="error-container">
                                <ErrorMessage name="password" component="div" className="error" />
                            </div>
                        </div>
                        <div className="field">
                            <label>{i18n.t('ConfirmPassword')}</label>
                            <Field name="confirm_password" type="password" onChange={handleChange}
                                   placeholder={i18n.t('NewPasswordPlaceholder')}/>
                            <div className="error-container">
                                <ErrorMessage name="confirm_password" component="div" className="error" />
                            </div>
                        </div>

                        <div style={{paddingTop: '20px'}}>
                            <button className="ui button orange" type="submit" disabled={isSubmitting}
                                    style={{width: '100%'}}>{i18n.t('ResetButton')}</button>
                        </div>

                    </form>
                )}
            </Formik>
        )
    }

    render() {
        return (
            <div>
                <h3 className="ui center aligned header">{i18n.t('ResetYourPass')}</h3>
                { this.state.alreadyReset ? this.renderPasswordReset() : this.renderPasswordForm() }
            </div>
        );
    }
}

export default ResetPassword
