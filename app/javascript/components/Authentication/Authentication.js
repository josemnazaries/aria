import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import Login from './Login.jsx';
import RememberPassword from "./RememberPassword";
import ResetPassword from "./ResetPassword";
import ContactAdmin from "./ContactAdmin";

class Authentication extends React.Component {
    render() {
        return (
            <Router>
                <div className="ui segment grid middle aligned" style={{height: '100vh', margin: '0'}}>
                    <div className="ui main column container">
                        <div className="ui one column centered grid">
                            <div className="ui raised very padded segment" style={{width: '450px'}}>
                                <Switch>
                                    <Route path='/remember'>
                                        <RememberPassword {...this.props}/>
                                    </Route>
                                    <Route path='/reset_password'>
                                        <ResetPassword {...this.props}/>
                                    </Route>
                                    <Route path='/contact_admin'>
                                        <ContactAdmin {...this.props}/>
                                    </Route>
                                    <Route>
                                        <Login {...this.props}/>
                                    </Route>
                                </Switch>
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}

export default Authentication;
