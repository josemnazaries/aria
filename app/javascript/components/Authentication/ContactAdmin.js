import React from "react"
import i18n from "../i18n";
import {ErrorMessage, Field, Formik} from "formik";
import * as Yup from "yup";
import { Link, Redirect } from "react-router-dom";
import axios from "../../services/axios";

class ContactAdmin extends React.Component {

    state = {
        sent: true,
        error: null
    };

    onFormSubmit = (values, submittingFormik) => {
        axios.post('/api/contact_admin', values)
            .then( () => {
                this.setState({
                    error: null,
                    sent: true
                });
                submittingFormik(false);
            }).catch(error => {
                this.setState({error: error.response.data.message});
                submittingFormik(false);
        })
    };

    errorMessage() {
        return this.state.error ? <p className="error">{this.state.error}</p> : null;
    }

    render_contact_admin_form() {
        return (
            <div>
                <h3 className="ui center aligned header">{i18n.t('ContactAdmin')}</h3>

                {this.errorMessage()}

                <Formik onSubmit={ (values, { setSubmitting }) => { this.onFormSubmit(values, setSubmitting) } }
                        validationSchema={Yup.object().shape({
                            email: Yup.string()
                                .email(i18n.t('EmailInvalidValidation'))
                                .required(i18n.t('EmailRequiredValidation'))
                        })}>
                    {({
                          handleChange,
                          handleSubmit,
                          isSubmitting
                      }) => (
                        <form className="ui form container" onSubmit={handleSubmit} style={{paddingTop: '25px', paddingBottom: '25px'}}>
                            <div className="field">
                                <label>{i18n.t('Email')}</label>
                                <Field name="email" type="email" onChange={handleChange}
                                       placeholder={i18n.t('LoginEmailPlaceholder')}/>
                                <div className="error-container">
                                    <ErrorMessage name="email" component="div" className="error" />
                                </div>
                            </div>

                            <div style={{paddingTop: '20px'}}>
                                <button className="ui button orange" type="submit" disabled={isSubmitting}
                                        style={{width: '100%'}}>{i18n.t('ResetButton')}</button>
                            </div>

                        </form>
                    )}
                </Formik>

                <div className="ui center aligned text"
                     style={{textAlign: 'center', paddingTop: '100px', paddingBottom: '25px'}}>
                    <Link to="/" className="orange">{i18n.t('BackLogin')}</Link>
                </div>
            </div>
        );
    }

    render_contact_sent() {
        return (
            <div>
                <h3 className="ui center aligned header">{i18n.t('ContactAdminSent')}</h3>

                <div style={{paddingTop: '50px', paddingBottom: '50px'}}>
                    <Link to={"/"} className="ui button orange" style={{width: '100%'}} >{i18n.t('OkButton')}</Link>
                </div>
            </div>
        )
    }

    render () {
        return this.state.sent ? this.render_contact_sent() : this.render_contact_admin_form();
    }
}

export default ContactAdmin
