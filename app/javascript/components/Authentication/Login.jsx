import React from "react";
import i18n from "i18next";
import {Link} from "react-router-dom";
import axios from "../../services/axios";
import {Formik, Field, ErrorMessage} from "formik";
import * as Yup from 'yup';
import { credentialsKey } from '../../services/axios'

class Login extends React.Component {
    state = {
        error: null,
    };

    onFormSubmit = (values, submittingFormik) => {
        axios.post('/api/users', values)
            .then(response => {
                let userDetails = response.data.user;
                localStorage.setItem(credentialsKey, JSON.stringify(userDetails));
                this.props.logIn(userDetails);
                submittingFormik(false);
            }).catch(error => {
                this.setState({error: 'Either the email or password that you entered were incorrect'});
                submittingFormik(false);
        })
    };

    errorMessage() {
        return this.state.error ? <p className="error">{this.state.error}</p> : null;
    }

    render_login_form() {
        return (
            <div>
                <h3 className="ui center aligned header">{i18n.t("Login")}</h3>

                <div style={{minHeight: '50px', paddingBottom: '10px'}}>
                    {this.errorMessage()}
                </div>

                <Formik
                    onSubmit={(values, {setSubmitting}) => { this.onFormSubmit(values, setSubmitting) }}
                    initialValues={{
                        email: '',
                        password: ''
                    }}
                    validationSchema={Yup.object().shape({
                        email: Yup.string()
                            .email(i18n.t('EmailInvalidValidation'))
                            .required(i18n.t('EmailRequiredValidation')),
                        password: Yup.string()
                            .required(i18n.t('PasswordRequiredValidation')),
                    })}>
                    {({
                          values,
                          handleChange,
                          handleSubmit,
                          isSubmitting
                      }) => (
                        <form onSubmit={handleSubmit} className="ui form container" style={{paddingBottom: '100px'}}>
                            <div className="field">
                                <Field name="email" onChange={handleChange} type="text"
                                       placeholder={i18n.t('LoginEmailPlaceholder')}/>
                                <div className="error-container" >
                                    <ErrorMessage name="email" component="div" className="error" />
                                </div>
                            </div>
                            <div className="field">
                                <Field name="password" onChange={handleChange} type="password"
                                       placeholder={i18n.t('PasswordPlaceholder')}/>
                                <div className="error-container">
                                    <ErrorMessage name="password" component="div" className="error" />
                                </div>
                            </div>

                            <div className="ui right aligned" style={{textAlign: 'right', paddingBottom: '50px'}}>
                                <Link to="/remember">{i18n.t('ForgotPassword')}</Link>
                            </div>

                            <button className="ui button orange" type="submit" style={{width: '100%'}}
                                    disabled={isSubmitting}>{i18n.t('LoginButton')}</button>
                        </form>
                    )}
                </Formik>

                <div className="ui center aligned text" style={{textAlign: 'center', paddingBottom: '25px'}}>
                    <p>{i18n.t('NotAccountYet')}</p>
                    <Link to="/contact_admin">{i18n.t('ContactAdmin')}</Link>
                </div>
            </div>
        )
    }

    render() {
        return this.render_login_form();
    }
}

export default Login;