import React from "react"
import i18n from "../i18n"

import {initDropDown} from "../../semantic-ui";
import Axios from "../../services/axios";
import Toast from "../../services/toasts";

class ItemRow extends React.Component {
    componentDidMount() {
        initDropDown();
    }

    deleteItem(id) {
        Axios.delete(`api/items/${id}`)
            .then(() => {
                Toast.successToast(i18n.t('Communications'), i18n.t('ItemsToastDeleted'));
                this.props.loadItems();
            })
            .catch( () => {
                Toast.failureToast(i18n.t('Communications'), i18n.t('ItemsToastNotDeleted'));
            })
    }

    render() {
        return (
            <tr>
                <td data-label="Status">{this.props.item.extra_items.status}</td>
                <td data-label="Loaded">{this.props.item.extra_items.loaded || 'null'}</td>
                <td data-label="Queue ID">{this.props.item.extra_items.queue_name || 'null'}</td>
                <td data-label="Key Value">{this.props.item.extra_items.keyvalue || 'null'}</td>
                <td data-label="Process Name">{this.props.item.extra_items.process_name || 'null'}</td>
                <td data-label="Last Updated">{this.props.item.extra_items.lastupdated || 'null'}</td>
                <td data-label="Exception Reason">{this.props.item.extra_items.exceptionreason || ''}</td>
                <td className="ui center aligned">
                    <div className="ui dropdown">
                        <i className="icon ellipsis vertical"/>
                        <div className="menu">
                            <div className="item"
                                 onClick={() => this.deleteItem(this.props.item.id)}>{i18n.t('Delete')}</div>
                        </div>
                    </div>
                </td>
            </tr>
        );
    }
}

export default ItemRow
