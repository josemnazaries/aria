import React from "react"
import i18n from "../i18n"
import ItemsTable from './ItemsTable'
import axios from "../../services/axios"
import { Redirect } from 'react-router-dom'
import Permissions from "../../services/permissions"
import { VisitableComponents } from "../../services/permissionsRules"

class Activities extends React.Component {
    componentDidMount() {
        this.loadItems();
    }

    loadItems() {
      axios.get('/api/items')
          .then(response => {
              this.props.loadItems(response.data);
          }).catch(() => {
              this.props.loadItems([]);
          })
  }

  renderTable() {
    return (
        <div>
          <div className="fullscreen">
            <h3>{ i18n.t('Activities') }</h3>
          </div>

          <div className="ui segment" style={{marginRight: '10px'}}>
            {<ItemsTable items={this.props.items} loadItems={this.loadItems}/>}
          </div>

        </div>
    );
  }

  renderComponent(){
      return this.renderTable();
  }

  render(){
      return (
          <Permissions
              action={ [VisitableComponents.Activities, 'show'] }
              yes={() => {
                  return this.renderComponent();
              }}
              no={() => {
                  return <Redirect to='/'/>
              }}
          />
      );
  }
}

export default Activities
