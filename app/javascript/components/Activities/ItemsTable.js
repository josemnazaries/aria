import React from "react"
import i18n from "../i18n"

import ItemRow from './ItemRow'

class ItemsTable extends React.Component {
    renderEmpty = () => {
        return (
            <div className="ui" style={{padding: '40px'}}>
                <h4 style={{textAlign: 'center'}}>{ i18n.t('NoMessagesYet') }</h4>
            </div>
        );
    };

    renderLoading = () => {
        return (
            <div className="ui" style={{minHeight: '100px'}}>
                <div className="ui active inverted dimmer">
                    <div className="ui text loader">{ i18n.t('Loading') }</div>
                </div>
            </div>
        );
    };

    renderTable() {
        return (
            <table className="ui table">
                <thead>
                    <tr>
                        <th>{i18n.t('Status')}</th>
                        <th>{i18n.t('Loaded')}</th>
                        <th>{i18n.t('QueueName')}</th>
                        <th>{i18n.t('KeyValue')}</th>
                        <th>{i18n.t('ProcessName')}</th>
                        <th>{i18n.t('LastUpdated')}</th>
                        <th>{i18n.t('ExceptionReason')}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    { this.props.items.map(item => <ItemRow key={item.id} item={item} loadItems={this.props.loadItems}/>) }
                </tbody>
            </table>
        );
    }

    render() {
        if(this.props.items === undefined) {
            return this.renderLoading();
        } else {
            if(this.props.items.length > 0){
                return this.renderTable();
            } else {
                return this.renderEmpty();
            }
        }
    }
}

export default ItemsTable
