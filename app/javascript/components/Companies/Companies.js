import React from "react"
import { Redirect } from "react-router-dom"

import CompaniesTable from "./CompaniesTable"
import CompaniesModal, {ModalType} from "./CompaniesModal"
import {openModal} from "../../semantic-ui"
import i18n from "i18next"
import axios from "../../services/axios"
import Permissions from "../../services/permissions"
import {VisitableComponents} from "../../services/permissionsRules"

class Companies extends React.Component {
    componentDidMount() {
        this.loadCompanies();
    }

    constructor(props) {
        super(props);
        this.state = {modalType: ModalType.New, company: {}};
        this.showModal = this.showModal.bind(this)
    }

    showModal(modalType, company = {}) {
        this.setState({
            modalType: modalType,
            company: company
        });

        openModal()
    }

    loadCompanies = () => {
        this.setState({
            loading: true
        });

        axios.get('/api/companies')
            .then(response => {
                this.setState({loading: false});
                this.props.loadCompanies(response.data);
            }).catch(() => {
                this.setState({loading: false});
                this.props.loadCompanies([]);
            })
    };

    renderComponent() {
        return (
            <div>
                <div className="fullscreen">
                    <h3>{i18n.t('Companies')}</h3>
                </div>

                <div className="ui segment" style={{marginRight: '10px'}}>
                    {<CompaniesTable companies={this.props.companies} showModal={this.showModal}/>}
                    <button className="ui button orange"
                            onClick={() => this.showModal(ModalType.New)}>{i18n.t("AddCompanyButton")}</button>
                </div>

                <CompaniesModal jwt={this.props.jwt} company={this.state.company} modalType={this.state.modalType}
                                loadCompanies={this.loadCompanies}/>
            </div>
        );
    }

    render() {
        return (
            <Permissions
                action={ [ VisitableComponents.Companies, 'show' ] }
                yes={() => {
                    return this.renderComponent();
                }}
                no={() => {
                    return <Redirect to='/'/>
                }}
            />
        );
    }
}

export default Companies
