import React from "react"
import i18n from "i18next";
import axios from "../../services/axios";
import {Formik} from "formik";

import {closeModal} from "../../semantic-ui";

class CompaniesForm extends React.Component {

    formText = () => {
        return this.props.company.id ? 'EditCompanyInfo' : 'AddCompanyInfo';
    };

    submitText = () => {
        return this.props.company.id ? 'SaveButton' : 'AddButton';
    };

    createCompany = (values, submittingFormik) => {
        values["active"] = true;

        axios.post('api/companies', values)
            .then(() => {
                this.props.loadCompanies();
                submittingFormik(false);
                closeModal();
            })
            .catch(error => {
                submittingFormik(false);
                console.log('createCompany', error)
            })
    };

    updateCompany = (values, submittingFormik) => {
        axios.put(`api/companies/${this.props.company.id}`, values)
            .then(() => {
                this.props.loadCompanies();
                submittingFormik(false);
                closeModal();
            })
            .catch(error => {
                console.log('updateCompany', error)
            })
    };

    renderButtons(isSubmitting) {
        return (
            <div className="ui text two column centered grid" style={{paddingBottom: '25px'}}>
                <div className="row">
                    <div className="eight wide column">
                        <button className="ui button orange" style={{width: '100%'}} type="submit"
                                disabled={isSubmitting}>{i18n.t(this.submitText())}</button>
                    </div>
                    {this.props.company.id ? (
                        <div className="eight wide column">
                            <button className="ui button basic orange" style={{width: '100%'}} type="button"
                                    onClick={() => closeModal()}>{i18n.t('CancelButton')}</button>
                        </div>
                    ) : (null)}
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className="content">
                <p style={{textAlign: 'center'}}>{i18n.t(this.formText())}</p>

                <Formik
                    initialValues={{
                        name: this.props.company.name || '',
                        service_url: this.props.company.service_url || '',
                        admin_email: this.props.company.admin_email || '',
                        description: this.props.company.description || '',
                    }}
                    enableReinitialize={true}
                    onSubmit={(values, {setSubmitting}) => {
                        this.props.company.id ? this.updateCompany(values, setSubmitting) : this.createCompany(values, setSubmitting);
                    }}
                >
                    {({
                          values,
                          handleChange,
                          handleSubmit,
                          isSubmitting,
                      }) => (
                        <form className="ui form container" onSubmit={handleSubmit}>
                            <div className="field">
                                <label>{i18n.t('CompanyName')}</label>
                                <input name="name" onChange={handleChange}
                                       placeholder={i18n.t('CompanyNamePlaceholder')} value={values.name}/>
                            </div>
                            <div className="field">
                                <label>{i18n.t('ServiceUrl')}</label>
                                <input name="service_url" onChange={handleChange} placeholder={i18n.t('ServiceUrlPlaceholder')}
                                       value={values.service_url}/>
                            </div>
                            <div className="field">
                                <label>{i18n.t('Email')}</label>
                                <input name="admin_email" onChange={handleChange}
                                       placeholder={i18n.t('EmailPlaceholder')} value={values.admin_email}/>
                            </div>
                            <div className="field">
                                <label>{i18n.t('Description')}</label>
                                <input name="description" onChange={handleChange}
                                       placeholder={i18n.t('DescriptionPlaceholder')} value={values.description}/>
                            </div>

                            {this.renderButtons(isSubmitting)}
                        </form>
                    )}
                </Formik>

            </div>
        )
    }
}

export default CompaniesForm