import React from "react"
import i18n from "i18next";

import {ModalType} from "./CompaniesModal";
import {initDropDown} from "../../semantic-ui";

class CompanyRow extends React.Component {
    componentDidMount() {
        initDropDown();
    }

    render() {
        return (
            <tr>
                <td data-label="Company Name" style={{height: '50px'}}>
                    <a href='#' onClick={() => this.props.showModal(ModalType.Show, this.props.company)}>{this.props.company.name}</a>
                </td>
                <td data-label="Description">{this.props.company.description}</td>
                <td data-label="Team">-</td>
                <td data-label="E-mail">{this.props.company.admin_email}</td>
                <td className="ui center aligned">
                    <div className="ui dropdown">
                        <i className="icon ellipsis vertical"/>
                        <div className="menu">
                            <div className="item"
                                 onClick={() => this.props.showModal(ModalType.Show, this.props.company)}>{i18n.t('Info')}</div>
                            <div className="item"
                                 onClick={() => this.props.showModal(ModalType.Edit, this.props.company)}>{i18n.t('Edit')}</div>
                            <div className="item"
                                 onClick={() => this.props.showModal(ModalType.Suspend, this.props.company)}>{i18n.t('Suspend')}</div>
                        </div>
                    </div>
                </td>
            </tr>
        );
    }
}

export default CompanyRow
