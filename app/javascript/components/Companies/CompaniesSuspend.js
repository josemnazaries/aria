import React from "react"
import i18n from "i18next";
import axios from "../../services/axios";
import {closeModal} from "../../semantic-ui";

class CompaniesSuspend extends React.Component {

    noClick = () => {
        closeModal();
    };

    yesClick = () => {
        closeModal();
        axios.delete(`api/companies/${this.props.company.id}`)
            .then(response => {
                this.props.loadCompanies();
                closeModal();
            })
            .catch(error => {
                console.log('Suspend', error);
            })
    };

    render() {
        return (
            <div className="content">
                <p style={{textAlign: 'center'}}>{i18n.t('SuspendCompanyInfo')}</p>

                <div className="ui container">
                    <div className="ui text two column grid" style={{paddingBottom: '25px', paddingTop: '25px'}}>
                        <div className="row">
                            <div className="eight wide column">
                                <button className="ui button orange" style={{width: '100%'}}
                                        onClick={() => this.noClick()}>NO
                                </button>
                            </div>
                            <div className="eight wide column">
                                <button className="ui button basic orange" style={{width: '100%'}}
                                        onClick={() => this.yesClick()}>YES
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default CompaniesSuspend
