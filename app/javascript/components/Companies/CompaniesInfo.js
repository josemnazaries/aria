import React from "react"
import i18n from "i18next";

class CompaniesInfo extends React.Component {
    render() {
        return (
            <div className="content">
                <div className="ui two column grid">
                    <div className="row">
                        <p className="five wide column"><strong>{i18n.t('CompanyName')}:</strong></p>
                        <p className="eleven wide column">{this.props.company.name}</p>
                    </div>
                    <div className="row">
                        <p className="five wide column"><strong>{i18n.t('ServiceUrl')}:</strong></p>
                        <p className="eleven wide column">{this.props.company.service_url}</p>
                    </div>
                    <div className="row">
                        <p className="five wide column"><strong>{i18n.t('Email')}:</strong></p>
                        <p className="eleven wide column">{this.props.company.admin_email}</p>
                    </div>
                    <div className="row">
                        <p className="five wide column"><strong>{i18n.t('Processes')}:</strong></p>
                        <p className="eleven wide column">-</p>
                    </div>
                    <div className="row">
                        <p className="five wide column"><strong>{i18n.t('TeamSize')}:</strong></p>
                        <p className="eleven wide column">-</p>
                    </div>
                    <div className="row">
                        <p className="five wide column"><strong>{i18n.t('Description')}:</strong></p>
                        <p className="eleven wide column">{this.props.company.description}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default CompaniesInfo
