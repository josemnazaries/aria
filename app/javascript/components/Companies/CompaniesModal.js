import React from "react"
import i18n from "i18next";

import {closeModal} from "../../semantic-ui";
import CompaniesForm, {FormTypes} from "./CompaniesForm";
import CompaniesInfo from "./CompaniesInfo";
import CompaniesSuspend from "./CompaniesSuspend";

export const ModalType = Object.freeze({
    New: 'NEW',
    Edit: 'EDIT',
    Show: 'SHOW',
    Suspend: 'SUSPEND'
});

class CompaniesModal extends React.Component {
    modalTitle = () => {
        switch (this.props.modalType) {
            case (ModalType.New):
                return 'AddingCompany';
            case (ModalType.Edit):
                return 'EditingCompany';
            case(ModalType.Suspend):
                return 'SuspendCompany';
            case (ModalType.Show):
                return 'CompanyInfo';
        }
    };

    modalClass = () => {
        switch (this.props.modalType) {
            case (ModalType.New):
                return 'newCompanyModal';
            case (ModalType.Edit):
                return 'editCompanyModal';
            case (ModalType.Suspend):
                return 'suspendCompanyModal';
            case (ModalType.Show):
                return 'showCompanyModal';
        }
    };

    renderModalContent() {
        switch (this.props.modalType) {
            case (ModalType.New):
                return <CompaniesForm jwt={this.props.jwt} company={{}} loadCompanies={this.props.loadCompanies}/>;
            case (ModalType.Edit):
                return <CompaniesForm jwt={this.props.jwt} company={this.props.company} loadCompanies={this.props.loadCompanies}/>;
            case (ModalType.Suspend):
                return <CompaniesSuspend jwt={this.props.jwt} company={this.props.company} loadCompanies={this.props.loadCompanies}/>;
            case (ModalType.Show):
            default:
                return <CompaniesInfo company={this.props.company}/>;
        }
    }

    render() {
        return (
            <div className={"ui modal tiny " + this.modalClass()}>
                <div className="header ui center aligned text">
                    {i18n.t(this.modalTitle())}
                    <i className="close icon" style={{float: 'right', cursor: 'pointer'}}
                       onClick={() => closeModal()}/>
                </div>
                {this.renderModalContent()}
            </div>
        )
    }
}

export default CompaniesModal
