import React from "react"
import {I18nextProvider} from "react-i18next";
import i18n from "./i18n";
import NavigationBar from "./NavigationBar/NavigationBar";
import Authentication from "./Authentication/Authentication";
import {connect} from "react-redux";
import {logIn, logOut, loadCompanies, loadItems} from "../actions";
import {BrowserRouter as Router} from "react-router-dom";
import Dashboard from "./Dashboard/dashboard";
import { credentialsKey } from '../services/axios'

import { SemanticToastContainer } from 'react-semantic-toasts';
import 'react-semantic-toasts/styles/react-semantic-alert.css';

class AppContainer extends React.Component {
    componentDidMount() {
        const auth = localStorage.getItem(credentialsKey);

        if (auth){
            const parsedAuth = JSON.parse(auth);
            this.props.logIn(parsedAuth);
        }
    }

    render() {
        return (
            <I18nextProvider i18n={i18n}>
                <Router>
                    <NavigationBar logOut={this.props.logOut} auth={this.props.auth}/>
                    <SemanticToastContainer />
                    {this.props.auth.jwt ?
                        <Dashboard auth={this.props.auth}
                                   companies={this.props.companies.companies}
                                   loadCompanies={this.props.loadCompanies}
                                   items={this.props.items.items}
                                   loadItems={this.props.loadItems}
                        />
                        :
                        <Authentication logIn={this.props.logIn} auth={this.props.auth}/>
                    }
                </Router>
            </I18nextProvider>
        );
    }
}

const mapStateToProps = state => {
    const {auth, companies, items} = state;
    return {auth, companies, items};
};

export default connect(
    mapStateToProps,
    {logIn, logOut, loadCompanies, loadItems})
(AppContainer);