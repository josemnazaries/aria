import React from 'react'
import i18n from '../i18n'

import { Redirect } from "react-router-dom"
import Permissions from "../../services/permissions"
import {VisitableComponents} from "../../services/permissionsRules"

class Users extends React.Component {
    renderComponent() {
        return (
            <div className="fullscreen">
                <h3>{i18n.t('Users')}</h3>
            </div>
        );
    }

    render() {
        return (
            <Permissions
                action={ [VisitableComponents.Users,'show'] }
                yes={() => {
                    return this.renderComponent();
                }}
                no={() => {
                    return <Redirect to='/'/>
                }}
            />
        );
    }
}
export default Users
