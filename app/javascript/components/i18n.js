import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

import enTranslations from '../locales/en.json'

i18n.use(LanguageDetector).init({
    resources: {
        en: {
            translation: enTranslations
        }
    },
    lng: 'en',
    fallbackLng: "en",
    keySeparator: false,
    react: {
        wait: true
    }
});

export default i18n