import React from "react"
import MyImage from 'images/logo.png';
import { Redirect } from 'react-router-dom';

class NavigationBar extends React.Component {

    logOut() {
        localStorage.clear();
        this.props.logOut();
    }

    renderLogOutButton() {
        if (this.props.auth.jwt) {
            return (
                <div className="right menu">
                    <a className="item" onClick={() => this.logOut()}>Log out <i className="sign-out icon"/></a>
                </div>
            )
        } else {
            return null
        }
    }

    render() {
        return (
            <div className="ui menu segment" >
                <a style={{height: '40px'}}>
                    <img src={MyImage} alt="Invoke Logo" style={{height: '40px'}}/>
                </a>

                {this.renderLogOutButton()}
            </div>
        );
    }
}

export default NavigationBar;