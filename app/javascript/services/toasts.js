import { toast } from 'react-semantic-toasts';
import React from "react";

class Toast {
    static animation = 'fade down';
    static lifetime = 3000;

    static successToast = (title, description) => {
        toast(
            {
                title: title,
                description: <p>{description}</p>,
                size: "small",
                color: 'green',
                icon: 'check',
                animation: this.animation,
                time: this.lifetime,
                onClose: () => {},
                onClick: () => {},
                onDismiss: () => {}
            }
        );
    };

    static failureToast = (title, description) => {
        toast(
            {
                title: title,
                description: <p>{description}</p>,
                size: "small",
                color: 'red',
                icon: 'ban',
                animation: this.animation,
                time: this.lifetime,
                onClose: () => {},
                onClick: () => {},
                onDismiss: () => {}
            }
        );
    }
}

export default Toast;