import axios from 'axios'
import store from '../services/store'
import {logOut} from "../actions/index"

export const credentialsKey = 'userDetails';

const instance = axios.create({
    headers: {
        'Accept': 'application/json',
    }
});

instance.interceptors.request.use(config => {
    const auth = localStorage.getItem(credentialsKey);
    config.headers.authorization = auth ? `Bearer ${JSON.parse(auth).jwt}` : '';

    return config;
});

instance.interceptors.response.use(
    (response) => { return response; },
    (error) => {
        if(error.response.status === 401){
            store.dispatch(logOut());
        }
    }
);

export default instance;