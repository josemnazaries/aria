import getUserPermissions from './permissionsRules';

const check = (component, action) => {
    const permissions = getUserPermissions();

    return permissions && (
        permissions.includes(`${component}:${action}`) ||
        permissions.includes('all:manage')
    );
};

const Permissions = props => check(props.action) ? props.yes() : props.no();

Permissions.defaultProps = {
    yes: () => null,
    no: () => null
};

export default Permissions