import {credentialsKey} from "./axios"

export const VisitableComponents = Object.freeze({
    Activities: 'Activities',
    Companies: 'Company',
    Users: 'User',
    Settings: 'User'
});

const getUserPermissions = () => {
    const auth = localStorage.getItem(credentialsKey);
    const permissions = auth ? JSON.parse(auth).permissions : [];

    return permissions.map( (permission) => {
        return `${permission['subject_class']}:${permission['action']}`
    });
};

export default getUserPermissions;